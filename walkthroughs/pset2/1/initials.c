#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <cs50.h>

// prototypes
void trim(char* s);

int main(void)
{
    char* name = GetString();
    trim(name);
    
    char* out;
    
    char* tok = strtok(name, " ");
    while (tok != NULL) {
        // capitalize name
        char ch = isalpha(tok[0]) ? toupper(tok[0]) : tok[0];
        
        printf("%c", ch);
        
        tok = strtok(NULL, " ");
    }
    printf("\n");
    
}

/**
 * Trims a string
 * @see http://stackoverflow.com/a/123724
 */
void trim(char* s) {
    char * p = s;
    int l = strlen(p);

    while(isspace(p[l - 1])) p[--l] = 0;
    while(* p && isspace(* p)) ++p, --l;

    memmove(s, p, l + 1);
}
