#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <cs50.h>

int main(int argc, char *argv[])
{
    if (argc != 2) { 
        printf("Please, provide only one argument (key)\n");
        return 1;
    }
    
    char *key = argv[1];
    int keyStrLen = strlen(key);
    int keyIndex = 0;
    
    for (int i = 0; i < keyStrLen; i++) {
        if (!isalpha(key[i])) {
            printf("The key should contain alpha characters only\n");
            return 1;
        }
    }
    
    
    
    char *str = GetString();

    int strLen = strlen(str);
    char cypherStr[strLen + 1];
    for (int i = 0; i < strLen; i++) {
        int charCode = str[i];
        char cypherChar = str[i];
        
        // Encrypt only if it's an alpha char
        if (isalpha(str[i])) {
            int alphaCaseIndex = isupper(str[i]) ? 65 : 97;
            int charShift = charCode - alphaCaseIndex;
            int keyShift = tolower(key[keyIndex]) - 97;
            int shift = (charShift + keyShift) % 26;
            
            // converting ASCII char code to a char
            cypherChar = alphaCaseIndex + shift;
            
            if (++keyIndex == keyStrLen) keyIndex = 0;
        }

        cypherStr[i] = cypherChar;
        
        
        if (i == strLen - 1) cypherStr[i + 1] = '\0';
    }

    printf("%s\n", cypherStr);
}

