/**
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 */
       
#include <stdio.h>
#include <math.h>
#include <cs50.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
    if (n < 0) {
        printf("Incorrect size!\n");
        return false;
    }
    
    //
    // Performing binary search
    //
    
    // Instead of splitting the array and making a copy of a half of it
    // on every iteration, rather memorize the offset of a half.
    int offset = 0;
    int divider = n;
    while (divider > 0) {
        divider = round((divider + 1) / 2); // round up
        
        if (values[offset + divider] <= value) {
            if (offset + divider < n) {
                offset += divider;
            }
        }
        
        if (values[offset] == value) return true;
    }
    
    return false;
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    for (int i = 0; i < n; i++) {
        // find smallest value starting from the current offset, "i"
        int min = i;
        for (int j = i; j < n; j++) {
            if (values[j] < values[min]) {
                min = j;
            }
        }
        
        if (min > i) {
            // swap values (smallest value goes "i" west)
            int tmp = values[i];
            values[i] = values[min];
            values[min] = tmp;
        }
    }
}

