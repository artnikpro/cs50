#include <stdio.h>
#include <cs50.h>

int main(void)
{
    printf("Enter a number of rows to display\n");
 
    int value = GetInt();
    
    while (value < 0 || value > 23) {
        if (value < 0) printf("The minimum allowed number of rows is 23!\n");
        if (value > 23) printf("The maximum number of rows is 23!\n");
        value = GetInt();
    }

    for (int i = 0; i < value; i++) {
        printf("%*s", value - i - 1, ""); // spaces
        printf("%.*s", i + 2, "########################"); // #
        printf("\n");
    }
}
