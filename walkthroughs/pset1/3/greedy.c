#include <stdio.h>
#include <cs50.h>
#include <math.h>

int main(void)
{
    printf("How much change I owed?\n");

    float value = GetFloat();

    while (!value || value < 0) {
        printf("Invalid number, try again!\n");
        scanf("%f", &value);
    }

    int cents = round(value * 100); // converting dollars to cents
    
    int coinsAvailable[] = { 25, 10, 5, 1 }; // coins (in cents) available to give
    int currentCoinIndex = 0;

    int centsLeft = cents; // how much cents left to give
    int totalCoinsGiven = 0; // amount of coins already given
    while (centsLeft > 0) {
        int amount = coinsAvailable[currentCoinIndex];
 
        // retieve the miximum possible coin to give
        while (amount > centsLeft) {
            amount = coinsAvailable[++currentCoinIndex];
        }
 
        centsLeft -= amount;

        totalCoinsGiven++;
    }

    printf("%d\n", totalCoinsGiven);
}
