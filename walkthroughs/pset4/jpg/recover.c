/**
 * recover.c
 *
 * Computer Science 50
 * Problem Set 4
 *
 * Recovers JPEGs from a forensic image.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

int main(void)
{
    FILE *raw_fp = fopen("card.raw", "r");
    
    if (raw_fp == NULL) {
        fclose(raw_fp);
        printf("Could not open a file card.raw");
        return 1;
    }
    
    int buffer_size = 512;
    uint8_t buffer[buffer_size];
    int jpgcount = 0;
    bool found_file = false;
    FILE *jpg_fp;
    
    while (fread(buffer, buffer_size, 1, raw_fp) > 0) {
        // check if first 4 bytes is a beginning of a JPEG
        if (buffer[0] == 0xff && buffer[1] == 0xd8 && buffer[2] == 0xff 
            && (buffer[3] == 0xe0 || buffer[3] == 0xe1))
        {
            // finish with the previous file before processing the next one
            if (found_file) {
                fclose(jpg_fp);
            }
            
            found_file = true;
            
            // create a jpg file
            char filename[8];
            sprintf(filename, "%03d.jpg", jpgcount++);
            
            jpg_fp = fopen(filename, "w");
            
            if (jpg_fp == NULL) {
                fclose(jpg_fp);
                printf("Could not open %s for writing", filename);
                return 1;
            }
        }
        
        if (found_file) {
            fwrite(buffer, buffer_size, 1, jpg_fp);
            
            // close the jpg file if end of file is reached
            if (feof(jpg_fp)) fclose(jpg_fp);
        }
    }
    
    fclose(raw_fp);
}

